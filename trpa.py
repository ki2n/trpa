#!/usr/bin/python3.5

"""
	trpa 0.0.1
    by ki2ne [6/17/2016 23:37]

    trpa is a Discord bot written for railfanning communities on Discord.

    Requires Python 3.5+, and the latest Discord.py from Github. (pip3.5 install git+https://github.com/Rapptz/discord.py@async)
   
    TTS API provided by http://www.voicerss.org/

    Auth page: https://discordapp.com/oauth2/authorize?client_id=<id>&scope=bot&permissions=<perms_decimal>
    Perm Calc: https://discordapi.com/permissions.html

"""

import discord, time, datetime
from discord.ext import commands
import json, aiohttp

"""
    Predefined Stuff
"""
vcStations = {}
rbxUsers = {}

# remove when committing
ttsAPIKey = "9a1e8d3dfdf44592aceff801cf6fb780"  # Replace with the API key you get when registering an account with VoiceRSS.
ttsAnnounceLang = "en-us"                       # Replace if you wish to change the PA announcement language.
ttsAnnounceSpeed = -2                           # Default announcement speed. Default is -2 to be slow enough to be understood.
ttsFormat = "wav"

trpa = commands.Bot(command_prefix="!pa ")

# Check if Opus library is loaded. At the moment it's only configured for Linux.
if discord.opus.is_loaded() == False:
    discord.opus.load_opus("libopus.so.1")
#else:   Still need to work on voice stuff
    #ttsPA = discord.VoiceClient(trpa.user)
"""
    End Predefined Stuff
"""

# Link Start!
@trpa.event
async def on_ready():
    print("Trailways-PA is online. Start Time: %s" % time.strftime("%Y-%m-%d %H:%M:%S"))
    print('Linked to Discord - User: %s [%s]' % (trpa.user.name,trpa.user.id))
    print('------')

"""
    Administrative Commands
"""
@trpa.command(pass_context=True)
async def restoredb(ctx, database:str):
    if database == "roblox":
        print("[%s] restoredb(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), database, ctx.message.author.name))
        # load Roblox user db from JSON file
        await trpa.say("`Roblox user database restored.`")
    elif database == "stationvc":
        print("[%s] restoredb(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), database, ctx.message.author.name))
        # load voice channel db from JSON file
        await trpa.say("`Station channel database restored.`")
    else:
        print("[%s] restoredb(%s) invoked by %s, but failed: database does not exist." % (time.strftime("%Y-%m-%d %H:%M:%S"), database, ctx.message.author.name))
        await trpa.say("`Error: database not found.`")

@trpa.command(pass_context=True)
async def dumpdb(ctx, database:str):
    if database == "roblox":
        print("[%s] dumpdb(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), database, ctx.message.author.name))
        # save Roblox user data as JSON file
        await trpa.say("`Roblox user database saved.`")
    elif database == "stationvc":
        print("[%s] dumpdb(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), database, ctx.message.author.name))
        # save voice channels that are designated as a station (to receive TTS announcments)
        await trpa.say("`Station channel database saved.`")
    else:
        print("[%s] dumpdb(%s) invoked by %s, but failed: database does not exist." % (time.strftime("%Y-%m-%d %H:%M:%S"), database, ctx.message.author.name))
        await trpa.say("`Error: database not found.`")

"""
    End Administrative Commands
"""

"""
    Dispatcher Commands
"""

""" Temporarily disabled until voice channel feature is working.
# This is for when the preset announcements doesn't fit.
@trpa.command(pass_context=True)
async def announce(ctx, annmsg:str):
    with aiohttp.ClientSession() as session:
        async with session.get("https://api.voicerss.org/?key=%s&hl=%s&r=%d&format=%s&src=%s" % (ttsAPIKey, ttsAnnounceLang, ttsAnnounceSpeed, ttsFormat,annmsg)) as resp:
            data = await resp.read()
            #print(data)
            print(type(data))
"""
    
"""
    End Dispatcher Commands
"""





"""
    Roblox API
"""

"""
@trpa.command(pass_context=True)
async def dumpdb(ctx):
    
    print("[%s] dumpdb(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), username, ctx.message.author.name))
"""

@trpa.command(pass_context=True)
async def getrbxinfo(ctx, username):
    print("[%s] rbxstatus(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), username, ctx.message.author.name))



@trpa.command(pass_context=True)
async def rbxstatus(ctx, username):
    print("[%s] rbxstatus(%s) invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), username, ctx.message.author.name))
    #fix later
    with aiohttp.ClientSession() as session:
        async with session.get("https://api.roblox.com/users/get-by-username?username=%s" % username) as resp:
            json_data = await resp.json()
            
            if "IsOnline" in json_data:
                if  json_data["IsOnline"] == True:
                    await trpa.say("`%s [%d] is online.`" % (json_data["Username"], json_data["Id"]))
                else:
                    await trpa.say("`%s [%d] is offline.`"% (json_data["Username"], json_data["Id"]))
            else:
                await trpa.say("Sorry, that user is not found.")
           
        
"""
@trpa.command(pass_context=True)
async def getRbxName(ctx):
    
    print("[%s] getRbxName() invoked by %s." % (time.strftime("%Y-%m-%d %H:%M:%S"), ctx.message.author.name))
    #fix later
    with aiohttp.ClientSession() as session:
        async with session.get("http://www.roblox.com/UserCheck/get-by-username?username=%s" % username) as resp:
            json_data = await resp.json()
            if json_data["quoteText"][-1] == ' ': 
                await trpa.say("\"*%s*\" **—%s**" % (json_data["quoteText"][:-2], json_data["quoteAuthor"]))
            else:
                await trpa.say("\"*%s*\" **—%s**" % (json_data["quoteText"][:-1], json_data["quoteAuthor"]))
"""

"""
    End Roblox API
"""

trpa.run('MTkzNTczNzc2NTYxMDEyNzM2.CkZXIA.VwB6D1AXF1dHUx4357uwR_VefYE')
